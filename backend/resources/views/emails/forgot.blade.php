<meta charset="UTF-8">
<html style="padding:0;">
	<body style="padding:0; margin:0; font-family:arial">
		<div style="background-color:#0074df; padding:20px;">
			<div style="width:820px; margin:0 auto; padding:10px 0;">
			<img style="width:150px" src="http://static-course.thailivestream.com/assets/images/default/loading/landding-loading.png">
			</div>
			<div style="width:800px; background-color:#ffffff; margin:0 auto; padding:10px;">
				<h4>สวัสดี คุณ <?php echo $member['member_first_name']; ?> <?php echo $member['member_last_name']; ?>,</h4>

				<p style="font-size:14px; line-height:24px;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ท่านสามารถเปลี่ยนรหัสของท่านใหม่ได้ <a href="http://course.thailivestream.com/reset-password/<?php echo $member['member_remember_token']; ?>">ที่นี่</a> ค่ะ</p>

				<hr style="border:0; border-top:1px solid #0074df;"/>
				<table>
					<tr>
						<td style="width:400px">
							<h4 style="margin:0;">DooTV Media Co,. Ltd.</h4>
							<p style="font-size:12px; line-height:20px; margin-top:0;">
								4/4 M.11 Ratchaphruek Rd.<br/> 
								Bangprom TalingChan Bangkok 10170<br/> 
								Tel. 02-412-8880<br/>    
								Mobile : 083-600-9454<br/> 
								Email : <a href="mailto:support@thailivestream.com">support@thailivestream.com</a><br/> 
							</p>
						</td>
						<td style="width:400px; text-align:right;">
							<div style="display:inline-block;">Follow Us</spdivan>
							<ul style="list-style:none; display:inline-block; margin:0;">
								<li style="display:inline-block;">
									<a href="https://www.facebook.com/thailivestream">
										<img src="http://static-course.thailivestream.com/assets/images/default/email/facebook.png">
									</a>
								</li>
								<li style="display:inline-block;">
									<a href="https://www.youtube.com/channel/UC0Ij1XMOpvADhZk459iKAGA">
										<img src="http://static-course.thailivestream.com/assets/images/default/email/youtube.png">
									</a>
								</li>
							</ul>
						</td>
					</tr>
				</table>
			</div>
			<div style="width:800px; background-color:#2a2a29; margin:0 auto; padding:10px; color:#828282; font-size:12px; line-height:24px;">
				Powered by thailivestream.com © 2015 Thai Livestream. All rights reserved
			</div>
		</div>
	</body>
</html>