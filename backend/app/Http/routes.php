<?php

/** © 2015 All Rights Reserved - Sirirat Saemak [BEST] */

Route::group(array('prefix'=>'/api/'), function() {

	// hotel
    Route::get('hotels', 'HotelController@getHotels');
    Route::get('hotel/{hotel_id}', 'HotelController@getHotelInfo');

    // room
    Route::get('room/{room_id}', 'RoomController@getRoomInfo');
    Route::get('search', 'RoomController@searchAvailableRoom');

    // booking
    Route::post('booking', 'BookingController@BookingRoom');
    Route::get('booking/{booking_id}', 'BookingController@getBookingInfo');

});
