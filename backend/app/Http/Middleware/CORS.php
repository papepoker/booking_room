<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\Middleware;
use Illuminate\Http\Response;

class CORS  {

 /**
  * Handle an incoming request.
  *
  * @param \Illuminate\Http\Request $request
  * @param \Closure $next
  * @return mixed
  */
    public function handle($request, Closure $next) {

        $http_origin = "";
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            $http_origin = $_SERVER['HTTP_ORIGIN'];
        }

        $allowed_http_origins = array(
            "http://128.199.95.64:8001",  
        );

        if (in_array($http_origin, $allowed_http_origins)) {
            return $next($request)->header('Access-Control-Allow-Origin' , $http_origin)
                  ->header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE')
                  ->header('Access-Control-Allow-Credentials', 'true')
                  ->header('Access-Control-Allow-Headers', 'Content-Type,x-requested-with,Authorization,Access-Control-Allow-Origin');
        } else {
            return $next($request);
        }
    }
}