<?php
/** © 2015 All Rights Reserved - Sirirat Saemak [BEST] */

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Room;
use DB;
use Input;

use Illuminate\Http\Request;

class RoomController extends Controller {

    public function getRoomInfo($room_id) {
        $room = Room::find($room_id); 
        $room->hotel = $room->hotel()->get(); 
        return response()->json($room->toArray(), 200);
    }  

    public function searchAvailableRoom(Request $request) {
        $startDate = $request->input('startDate', '');
        $endDate = $request->input('endDate', '');
        $persons = $request->input('persons', 2);        
        $pageNo = $request->input('pageNo', 1);
        $pageLimit = $request->input('pageLimit', 20);
        $orderBy = $request->input('orderBy', 'room_id');
        $orderDirection = $request->input('orderDirection', 'ASC');
        $skip = ($pageNo-1)*$pageLimit;

        $room_list = DB::table('room')  
        ->where('hotel_id', '=', $hotel_id)
        ->where('room_available_status', '=', 1)            
        ->orderBy($orderBy, $orderDirection);

        $count = $room_list->count();
        $data = $room_list->skip($skip)->take($pageLimit)->get();

        if(!empty($data)) {
            echo $this->_successResponse($data, true, $count, $pageNo, $pageLimit);
        } else {
            echo $this->_statusResopnse(204);
        }
    }     

}