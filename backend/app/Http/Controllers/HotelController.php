<?php
/** © 2015 All Rights Reserved - Sirirat Saemak [BEST] */

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Hotel;
use DB;
use Input;

use Illuminate\Http\Request;

class HotelController extends Controller {

    public function getHotels(Request $request)
    {
        $pageNo = $request->input('pageNo', 1);
        $pageLimit = $request->input('pageLimit', 20);
        $orderBy = $request->input('orderBy', 'hotel_rate');
        $orderDirection = $request->input('orderDirection', 'DESC');
        $skip = ($pageNo-1)*$pageLimit;

        $hotel_list = DB::table('hotel')   
        ->where('hotel_status', '=', 1)               
        ->orderBy($orderBy, $orderDirection);

        $count = $hotel_list->count();
        $data = $hotel_list->skip($skip)->take($pageLimit)->get();

        if(!empty($data)) {
            echo $this->_successResponse($data, true, $count, $pageNo, $pageLimit);
        } else {
            echo $this->_statusResopnse(204);
        }
    }  

    public function getHotelInfo($hotel_id) {
        $hotel = Hotel::find($hotel_id); 
        $hotel->room = $hotel->room()->get(); 
        return response()->json($hotel->toArray(), 200);
    }       

}