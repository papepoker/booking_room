<?php
/** © 2015 All Rights Reserved - Sirirat Saemak [BEST] */

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\BookingItem;
use App\Models\Room;
use DB;
use Input;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BookingController extends Controller {

    public function BookingRoom(Request $request) {
        $totalPrice = 0;

        $booking = new Booking();

        $validator = Validator::make(Input::all(), Booking::$create_rule, Booking::$messages);
        if($validator->fails()) {
            echo $this->_statusResopnse(400 , $validator->messages());
        } else {

            foreach (Input::all() as $key => $value) {
                if (in_array($key, $booking->getFillable())) {
                    if($key == 'booking_start_date' || $key == 'booking_end_date') {
                        $booking->$key = Carbon::parse($value)->toDateString(); 
                    } else {
                        $booking->$key = $value; 
                    }             
                }
            }  

            $startDate = Carbon::parse($booking->booking_start_date);
            $endDate = Carbon::parse($booking->booking_end_date);

            // total difference day
            $totalDay = $startDate->diffInDays($endDate);
       
            if($booking->save()) {
                $room = Room::find($booking->room_id);
                for ($x = 0; $x <= $totalDay; $x++) {

                    $booking_item = new BookingItem();
                    $booking_item->booking_id = $booking->booking_id;
                    // loop get day for booking
                    if($x == 0) {
                        $booking_item->booking_item_date = $startDate;
                    } else {
                        $booking_item->booking_item_date = $startDate->addDays(1);
                    }
                    // checking Day of week
                    if(Carbon::parse($booking_item->booking_item_date)->dayOfWeek == 6 || Carbon::parse($booking_item->booking_item_date)->dayOfWeek == 0) {
                        $booking_item->booking_item_high_season = 1;
                        $booking_item->booking_item_price = $room->room_high_season_price;
                    } else {
                        $booking_item->booking_item_high_season = 0;
                        $booking_item->booking_item_price = $room->room_nomal_price;
                    }

                    // total price
                    $totalPrice = $totalPrice + $booking_item->booking_item_price;
                    $booking_item->save();                 
                }

                // Save total price
                $booked = Booking::find($booking->booking_id); 
                $booked->booking_total_price = $totalPrice;
                if($booked->save()) {
                    return response()->json($booked->toArray(), 200);
                }  
            }
        }  
    }   

    public function getBookingInfo($booking_id) {
        $booking = Booking::find($booking_id); 
        $booking->item = $booking->booking_item()->get(); 
        return response()->json($booking->toArray(), 200);
    }     

}