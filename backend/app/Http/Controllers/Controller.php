<?php 
/** © 2015 All Rights Reserved - Sirirat Saemak [BEST] */

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

	  use DispatchesCommands, ValidatesRequests;  

        public function array_fetch($data_list) {
            $result = array();
            
              foreach ($data_list as $data) {
                foreach ($data as $key => $value) {
                    $sub[$key] = $value;
                }
                $result[] = $sub;
              }
            
            return $result;
        }  	

        protected function _successResponse($data, $fetch = false, $totalItems = NULL, $pageNo = 1, $pageLimit = 1) {           

            if($fetch == true) {
              $data = $this->array_fetch($data);
            }

            if(!empty($totalItems)) {
                $pagination = array(                               
                  'totalPages' => ceil($totalItems / $pageLimit),
                  'currentPage' => (int)$pageNo,  
                  'itemPerPage' => (int)$pageLimit,
                  'totalItems' => $totalItems,
                  'itemInPage' => count($data),
                );  
                            
                $response = $this->_response($data, 200 ,$pagination);           
            } else {
                $response = $this->_response($data, 200);
            }            
          
            return json_encode($response);
        }	

    	 protected function _statusResopnse($code, $field = NULL) {                                 
            if(!empty($field)) {
                $error = array(
                    'code' => $code,
                    'message' => $this->_getStatusCodeMessage($code),
                    'field' => $field
                ); 
            } else {
                $error = array(
                    'code' => $code,
                    'message' => $this->_getStatusCodeMessage($code)
                ); 
            }           

            $response = $this->_response($error, $code);

            return json_encode($response);
        }	

        protected function _response($data, $code, $pagination = NULL) {          
            if(!empty($pagination)) {
              return array(
                  'pagination' => $pagination,
                  'data' => $data
              );            
            } else {
              return array(
                  'data' => $data
              );
            }
        } 	

        protected function _getStatusCodeMessage($code) {
            $codes = array(
      	      100 => 'Continue',
      	      101 => 'Switching Protocols',
      	      200 => 'OK',
      	      201 => 'Created',
      	      202 => 'Accepted',
      	      203 => 'Non-Authoritative Information',
      	      204 => 'No Content',
      	      205 => 'Reset Content',
      	      206 => 'Partial Content',
      	      300 => 'Multiple Choices',
      	      301 => 'Moved Permanently',
      	      302 => 'Found',
      	      303 => 'See Other',
      	      304 => 'Not Modified',
      	      305 => 'Use Proxy',
      	      306 => '(Unused)',
      	      307 => 'Temporary Redirect',
      	      400 => 'Bad Request',
      	      401 => 'Unauthorized',
      	      402 => 'Payment Required',
      	      403 => 'Forbidden',
      	      404 => 'Not Found',
      	      405 => 'Method Not Allowed',
      	      406 => 'Not Acceptable',
      	      407 => 'Proxy Authentication Required',
      	      408 => 'Request Timeout',
      	      409 => 'Conflict',
      	      410 => 'Gone',
      	      411 => 'Length Required',
      	      412 => 'Precondition Failed',
      	      413 => 'Request Entity Too Large',
      	      414 => 'Request-URI Too Long',
      	      415 => 'Unsupported Media Type',
      	      416 => 'Requested Range Not Satisfiable',
      	      417 => 'Expectation Failed',
      	      500 => 'Internal Server Error',
      	      501 => 'Not Implemented',
      	      502 => 'Bad Gateway',
      	      503 => 'Service Unavailable',
      	      504 => 'Gateway Timeout',
      	      505 => 'HTTP Version Not Supported',

            );

            return (isset($codes[$code])) ? $codes[$code] : '';
        }
  
        protected function _buildErrors($field, $message) {
            return array(
              'field' => $field,
              'message' => $message,
            );
        }	

        protected function getClientIP() {

            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);

            return $iplist[0];
        }    	

}
