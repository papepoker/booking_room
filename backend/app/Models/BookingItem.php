<?php
/** © 2015 All Rights Reserved - Sirirat Saemak [BEST] */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class BookingItem extends Eloquent {

    const CREATED_AT = 'booking_item_create_datetime';
    const UPDATED_AT = 'booking_item_modify_datetime'; 

    protected $table = 'booking_item';
    protected $primaryKey = 'booking_item_id';

    protected $fillable = array('booking_id', 'booking_item_price', 'booking_item_date', 'booking_item_high_season');
    protected $guarded = array('booking_item_id');

    public static $create_rule  =  array(
        'booking_id' => 'required',
        'booking_item_price' => 'required|email',
        'booking_item_date' => 'required', 
        'booking_item_high_season' => 'required'
    ); 
}
