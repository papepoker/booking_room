<?php
/** © 2015 All Rights Reserved - Sirirat Saemak [BEST] */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Room extends Eloquent {

    public $timestamps = false;

    protected $table = 'room';
    protected $primaryKey = 'room_id';

    protected $fillable = array('hotel_id', 'room_name', 'room_available_status', 'room_nomal_price', 'room_high_season_price', 'room_person');
    protected $guarded = array('room_id'); 

    public function hotel()
    {
        return $this->belongsTo('App\Models\Hotel');
    }

}
