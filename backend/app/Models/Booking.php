<?php
/** © 2015 All Rights Reserved - Sirirat Saemak [BEST] */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Booking extends Eloquent {

    const CREATED_AT = 'booking_create_datetime';
    const UPDATED_AT = 'booking_modify_datetime'; 

    protected $table = 'booking';
    protected $primaryKey = 'booking_id';

    protected $fillable = array('booking_customer_name', 'booking_customer_email', 'booking_customer_mobile', 'room_id', 'booking_start_date', 'booking_end_date', 'booking_total_price', 'booking_status', 'payment_id');
    protected $guarded = array('booking_id');

    public static $create_rule  =  array(
        'booking_customer_name' => 'required',
        'booking_customer_email' => 'required|email',
        'booking_customer_mobile' => 'required', 
        'room_id' => 'required',
        'booking_start_date' => 'required',
        'booking_end_date' => 'required'
    ); 

    public static $messages  =  array(
        'booking_customer_name.required' => 'Please enter your name.',
        'booking_customer_email.required' => 'Please enter your email.',
        'booking_customer_email.email' => 'Must be a valid email address.',
        'booking_customer_mobile.required' => 'Please enter your phone number.',
        'room_id.required' => 'Please select room for booking.',
        'booking_start_date.required' => 'Please select start date.',
        'booking_end_date.required' => 'Please select end date.'
    );  

    public function booking_item()
    {
        return $this->hasMany('App\Models\BookingItem');
    }      

}
