<?php
/** © 2015 All Rights Reserved - Sirirat Saemak [BEST] */

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Hotel extends Eloquent {

    public $timestamps = false;

    protected $table = 'hotel';
    protected $primaryKey = 'hotel_id';

    protected $fillable = array('hotel_id', 'hotel_name', 'hotel_descrpition', 'hotel_rate', 'hotel_status');
    protected $guarded = array('hotel_id'); 

    public function room()
    {
        return $this->hasMany('App\Models\Room');
    }     

}
