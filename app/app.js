var app = angular.module('theApp', ['ngRoute', 'ui.bootstrap']);

app.config(function ($routeProvider, $locationProvider, $httpProvider) {

    $httpProvider.defaults.withCredentials = true;

    $routeProvider
        .when('/', {
            templateUrl: 'app/views/index.html',
            controller: 'HomeController'                        
        })
        .when('/hotels', {
            templateUrl: 'app/views/hotel/list.html',
            controller: 'HotelListController'                        
        })            
        .when('/hotel/:hotel_id', {
            templateUrl: 'app/views/hotel/index.html',
            controller: 'HotelInfoController'                        
        })                
        .when('/booking/complete/:booking_id', {
            templateUrl: 'app/views/booking/complete.html',
            controller: 'BookingCompleteController'                        
        })
        .when('/booking/:room_id', {
            templateUrl: 'app/views/booking/form.html',
            controller: 'BookingInfoController'                        
        })          
        .when('/booking/confirm/:booking_id', {
            templateUrl: 'app/views/booking/confirm.html',
            controller: 'BookingConfirmController'                        
        })         
        .when('/404', {
            templateUrl: 'app/views/errors/404.html'                      
        })                                                                                                                        
        .otherwise({
            redirectTo: '/404'
        });

    //$locationProvider.html5Mode(true);

});	

app.run(function($sce, $rootScope, $window, $location, $timeout, $modal, $modalStack) {
    $rootScope.BasePath = 'http://128.199.95.64:8001/backend/api/';
    $rootScope.BaseHost = 'http://128.199.95.64:8001/';
    $rootScope.CurrentUrl = $location.url();
    $rootScope.path = $location.path();
    $rootScope.FirstPath = $rootScope.path.split("/")[1];           
});
