app.controller('HotelListController', function ($window, $rootScope, $scope, $sce, $route, $location, HotelService) { 
        $scope.itemsPerPage = 9;
        $scope.sortingOrder = 'hotel_id';
        $scope.sortingDirection = 'desc';

        var query_string = "pageLimit="+$scope.itemsPerPage +"&orderBy="+$scope.sortingOrder+"&orderDirection="+$scope.sortingDirection;
        HotelService.getHotels(query_string).success(function(data) {
            $scope.hotel_list = data.data;
        });    
});
