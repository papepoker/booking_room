app.controller('BookingInfoController', function ($window, $routeParams, $rootScope, $scope, $sce, $route, $location, RoomService, BookingService) {
    $scope.booking = {};
    $scope.start_date = {opened: false};
    $scope.end_date = {opened: false};
    $scope.format = 'dd-MMMM-yyyy';

    $scope.today = function() {
        $scope.booking.booking_start_date = new Date();
        $scope.booking.booking_end_date = new Date();
    };
  
    $scope.today();

    $scope.toggleMin = function() {
        $scope.minDate = $scope.minDate ? null : new Date();
    };

    $scope.toggleMin();

    if(!angular.isUndefined($routeParams.room_id)) {
        RoomService.getRoomInfo($routeParams.room_id).success(function(data) {
            $scope.room = data;
            $scope.booking.room_id = $scope.room.room_id;
            $scope.hotel = data.hotel[0];
        }); 
    }     

    $scope.open_start_date = function($event) {
        $scope.start_date.opened = true;
    };

    $scope.open_end_date = function($event) {
        $scope.end_date.opened = true;
    };

    $scope.submitBooking = function(booking) {
        BookingService.BookingRoom(booking).success(function(data) {
            if(data.code != 400) {
                window.location.href = '#/booking/confirm/' + data.booking_id;
            }
        });          
    }



	       
});
