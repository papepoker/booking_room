app.controller('HotelInfoController', function ($window, $routeParams, $rootScope, $scope, $sce, $route, $location, HotelService) {  
    $scope.itemsPerPage = 4;
    $scope.sortingOrder = 'hotel_rate';
    $scope.sortingDirection = 'desc';
    $scope.booking = {};

    if(!angular.isUndefined($routeParams.hotel_id)) {
        HotelService.getHotelInfo($routeParams.hotel_id).success(function(data) {
            $scope.hotel = data;
            $scope.room_list = data.room;
        }); 
    }       

    var query_string = "pageLimit="+$scope.itemsPerPage +"&orderBy="+$scope.sortingOrder+"&orderDirection="+$scope.sortingDirection;
    HotelService.getHotels(query_string).success(function(data) {
        $scope.hotel_list = data.data;
    });   

});
