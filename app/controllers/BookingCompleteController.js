app.controller('BookingCompleteController', function ($window, $routeParams, $rootScope, $scope, $sce, $route, $location, BookingService, HotelService, RoomService) {
    $scope.itemsPerPage = 4;
    $scope.sortingOrder = 'hotel_rate';
    $scope.sortingDirection = 'desc';

    var query_string = "pageLimit="+$scope.itemsPerPage +"&orderBy="+$scope.sortingOrder+"&orderDirection="+$scope.sortingDirection;
    HotelService.getHotels(query_string).success(function(data) {
        $scope.hotel_list = data.data;
    });    

    if(!angular.isUndefined($routeParams.booking_id)) {
        BookingService.getBookingInfo($routeParams.booking_id).success(function(data) {
            $scope.booking = data;
            $scope.item_list = data.item;

            RoomService.getRoomInfo($scope.booking.room_id).success(function(data) {
                $scope.room = data;
                $scope.hotel = data.hotel[0];
            });             
        }); 
    }    
	       
});
