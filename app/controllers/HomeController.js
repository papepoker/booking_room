app.controller('HomeController', function ($window, $rootScope, $scope, $sce, $route, $location, HotelService) {  
        $scope.itemsPerPage = 3;
        $scope.sortingOrder = 'hotel_rate';
        $scope.sortingDirection = 'desc';

        var query_string = "pageLimit="+$scope.itemsPerPage +"&orderBy="+$scope.sortingOrder+"&orderDirection="+$scope.sortingDirection;
        HotelService.getHotels(query_string).success(function(data) {
            $scope.hotel_list = data.data;
        });   
});
