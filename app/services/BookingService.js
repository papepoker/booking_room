app.service('BookingService', ['$http', '$rootScope', '$filter', function($http, $rootScope, $filter) {
   return {
        BookingRoom: function(data) {

            if(!angular.isUndefined(data.booking_start_date)) {
                data.booking_start_date = $filter('date')(data.booking_start_date, 'yyyy-MM-dd');
            }  

            if(!angular.isUndefined(data.booking_end_date)) {
                data.booking_end_date = $filter('date')(data.booking_end_date, 'yyyy-MM-dd');
            }              

            return $http({
                url: $rootScope.BasePath + 'booking',
                method: "POST",
                data: $.param(data),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });
        },
        getBookingInfo : function(id) {           
            return $http({
                method: 'get',
                url: $rootScope.BasePath + 'booking/'+id,
            });
        }        									
   };

 }]);