app.service('HotelService', ['$http', '$rootScope', function($http, $rootScope ) {
   return {
      	getHotels : function(query_string) {			
			return $http({
				method: 'get',
				url: $rootScope.BasePath + 'hotels?'+query_string,
			});
		},
      	getHotelInfo : function(id) {			
			return $http({
				method: 'get',
				url: $rootScope.BasePath + 'hotel/'+id,
			});
		}				
   };

 }]);