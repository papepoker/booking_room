app.service('RoomService', ['$http', '$rootScope', function($http, $rootScope ) {
   return {
      	getRoomInfo : function(id) {			
			return $http({
				method: 'get',
				url: $rootScope.BasePath + 'room/'+id,
			});
		}  	
   };

 }]);