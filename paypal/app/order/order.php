<?php
require_once __DIR__ . '/../bootstrap.php';
$booking_id = $_GET['booking_id'];
	try {	
		$booking = getBooking($booking_id);
		if(!empty($booking)) {
			$baseUrl = getBaseUrl() . "return.php?booking_id=$booking_id";
			// generator product name
			$productName = "Booking : " . $booking['room_name'] . " Room for " . $booking['room_person'] . " persons [ " . $booking['booking_start_date'] . " - " . $booking['booking_end_date']. " ]"; 
			
			$payment = makePaymentUsingPayPal($booking['booking_total_price'], 'THB', $productName,
					"$baseUrl&success=true", "$baseUrl&success=false");
			updateBooking($booking_id, $payment->getState(), $payment->getId());		
			header("Location: " . getLink($payment->getLinks(), "approval_url"));
		}

	} catch (\PayPal\Exception\PPConnectionException $ex) {
		$message = parseApiError($ex->getData());
		$messageType = "error";
	} catch (Exception $ex) {
		$message = $ex->getMessage();
		$messageType = "error";
	}
?>

