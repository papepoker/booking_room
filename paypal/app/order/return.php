<?php
/*
 * Order completion page. When PayPal is used as the payment method,
 * the buyer gets redirected here post approval / cancellation of
 * payment.
 */
require_once __DIR__ . '/../bootstrap.php';
session_start();

if(isset($_GET['success'])) {

	// We were redirected here from PayPal after the buyer approved/cancelled
	// the payment
	
	if($_GET['success'] == 'true' && isset($_GET['PayerID']) && isset($_GET['booking_id'])) {
		$booking_id = $_GET['booking_id'];
		try {
			
			$booking = getBooking($booking_id);
			$payment = executePayment($booking['payment_id'], $_GET['PayerID']);
			if($payment->getState() == 'approved') {
			updateBooking($booking, $payment->getState());
            header('Location:'.retureUrl().$booking['booking_id']);		
			}

		} catch (\PayPal\Exception\PPConnectionException $ex) {
			echo $message = parseApiError($ex->getData());
			echo $messageType = "error";
		} catch (Exception $ex) {
			echo $message = $ex->getMessage();
			echo $messageType = "error";
		}
		
	} else {
		header('Location:/courses');
	}
}
