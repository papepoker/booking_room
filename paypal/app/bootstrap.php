<meta charset="UTF-8">
<?php

// Include the composer autoloader
if(!file_exists(__DIR__ .'/../vendor/autoload.php')) {
	echo "The 'vendor' folder is missing. You must run 'composer update' to resolve application dependencies.\nPlease see the README for more information.\n";
	exit(1);
}
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/common/booking.php';
require_once __DIR__ . '/common/paypal.php';

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;

// Define connection parameters
define('MYSQL_HOST', 'localhost');
define('MYSQL_USERNAME', 'root');
define('MYSQL_PASSWORD', 'iLove@Broker@');
define('MYSQL_DB', 'booking_room');

return getApiContext();

// SDK Configuration
function getBaseUrl() {
	return 'http://128.199.95.64:8001/paypal/app/order/';
}

function retureUrl() {
	return 'http://128.199.95.64:8001/#/booking/complete/';
}

function getApiContext() {


    // Define the location of the sdk_config.ini file
    if (!defined("PP_CONFIG_PATH")) {
        define("PP_CONFIG_PATH", dirname(__DIR__));
    }
	

    /* sandbox mode */
	$apiContext = new ApiContext(new OAuthTokenCredential(
		'ATiqKmTyUf1hVdvjfBzN7vka3jrXFjjOd3nzNYOEFSIwqgBdyvRB7uvC8USPvxom6CcqvbIQe2aN4gGG',
		'EN6b7vmaLHGWihE2qtsx3g_EX0uJFjmf-R5uy0ihDj5kTOTASA4JcbTlvmJUEdChgYkcB4xTZlyBEIaf'
	));

	return $apiContext;
}
