<?php

require_once __DIR__ . '/db.php';

function updateBooking($booking_id, $state, $paymentId=NULL) {
	$conn = getConnection();
	if($state == 'approved') { $state = 1; } else { $state = 0; }
	$query = sprintf("UPDATE `%s` SET `payment_id`='%s' , `booking_status`='%s' , `booking_pay_datetime`= NOW()  WHERE `booking_id`='%s'",
			BOOKING_TABLE,
			mysql_real_escape_string($paymentId),
			mysql_real_escape_string($state),
			mysql_real_escape_string($booking_id));

	$result = mysql_query($query, getConnection());

	if(!$result) {
		$errMsg = "Error updating booking record: " . mysql_error($conn);
		mysql_close($conn);
		throw new Exception($errMsg);
	}

	$isUpdated = mysql_affected_rows($conn);
	mysql_close($conn);
	
	return $isUpdated;
}

function getBooking($booking_id) {
	$conn = getConnection();
	$query = sprintf("SELECT * FROM `%s` left join room on booking.room_id = room.room_id WHERE `booking_id`='%s' ",
			BOOKING_TABLE,
			mysql_real_escape_string($booking_id));

	$result = mysql_query($query, $conn);
	if(!$result) {
		$errMsg = "Error retrieving order: " . mysql_error($conn);
		mysql_close($conn);
		throw new Exception($errMsg);
	}

	$row = mysql_fetch_assoc($result);
	mysql_close($conn);
	return $row;
}
?>