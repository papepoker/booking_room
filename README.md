# BOOKING ROOM SYSTEM (AngularJS , Laravel , Paypal REST API, MYSQL) #

Booking room system dev in one day.

### What is the structure file ###
* index.html at root path & "app" folder are develop by AngularJS
* "backend" folder is develop by Laravel (PHP Framework)
* "paypal" folder is develop by PHP & Paypal REST API

### How do I get set up? ###

1. Clone or download this repo
2. Create database (MYSQL) and import data with "schema.sql" in "database" folder
3. Change username & password in "backend/.env" , "backend/config/database.php" and "paypal/app/bootstrap.php"
4. go to "backend" folder with cmd and put this command "php artisan" for start laravel (For now you can access this url "http://localhost:8000/api/hotels")  
5. if you use web server "XAMPP". you can access website in your localhost Example : http://localhost:4000/booking_room/#/

### Account for test pay money with paypal ###

Email : booking_payment@gmail.com Password : booking2015