-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 17, 2015 at 03:51 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `booking_room`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE IF NOT EXISTS `booking` (
`booking_id` int(11) NOT NULL,
  `booking_customer_name` varchar(255) NOT NULL,
  `booking_customer_email` varchar(255) NOT NULL,
  `booking_customer_mobile` varchar(255) NOT NULL,
  `room_id` int(11) NOT NULL,
  `booking_start_date` date NOT NULL,
  `booking_end_date` date NOT NULL,
  `booking_total_price` int(11) NOT NULL,
  `booking_status` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `booking_pay_datetime` datetime NOT NULL,
  `booking_create_datetime` datetime NOT NULL,
  `booking_modify_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `booking_item`
--

CREATE TABLE IF NOT EXISTS `booking_item` (
`booking_item_id` int(11) NOT NULL,
  `booking_id` int(11) NOT NULL,
  `booking_item_price` int(11) NOT NULL,
  `booking_item_date` date NOT NULL,
  `booking_item_high_season` int(11) NOT NULL,
  `booking_item_create_datetime` datetime NOT NULL,
  `booking_item_modify_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `hotel`
--

CREATE TABLE IF NOT EXISTS `hotel` (
`hotel_id` int(11) NOT NULL,
  `hotel_name` varchar(255) NOT NULL,
  `hotel_descrpition` longtext NOT NULL,
  `hotel_rate` int(11) NOT NULL,
  `hotel_status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hotel`
--

INSERT INTO `hotel` (`hotel_id`, `hotel_name`, `hotel_descrpition`, `hotel_rate`, `hotel_status`) VALUES
(1, 'HOTEL A', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 3, 1),
(2, 'HOTEL B', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 5, 1),
(3, 'HOTEL C', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE IF NOT EXISTS `room` (
`room_id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `room_name` varchar(255) NOT NULL,
  `room_available_status` int(11) NOT NULL,
  `room_nomal_price` int(11) NOT NULL,
  `room_high_season_price` int(11) NOT NULL,
  `room_person` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`room_id`, `hotel_id`, `room_name`, `room_available_status`, `room_nomal_price`, `room_high_season_price`, `room_person`) VALUES
(1, 1, 'Standard', 1, 500, 1000, 2),
(2, 1, 'Deluxe', 1, 1500, 3000, 2),
(3, 0, 'Family', 1, 3000, 6000, 4),
(4, 2, 'Standard', 1, 800, 1600, 2),
(5, 1, 'Family', 1, 3000, 6000, 4),
(6, 2, 'Standard', 1, 800, 1600, 2),
(7, 2, 'Sweet', 1, 1500, 3000, 2),
(8, 2, 'Suite', 1, 1000, 2000, 2),
(9, 3, 'Family', 1, 3000, 6000, 5),
(10, 3, 'Deluxe', 1, 2000, 4000, 3),
(11, 3, '1 Bedroom', 1, 2500, 5000, 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
 ADD PRIMARY KEY (`booking_id`);

--
-- Indexes for table `booking_item`
--
ALTER TABLE `booking_item`
 ADD PRIMARY KEY (`booking_item_id`);

--
-- Indexes for table `hotel`
--
ALTER TABLE `hotel`
 ADD PRIMARY KEY (`hotel_id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
 ADD PRIMARY KEY (`room_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
MODIFY `booking_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `booking_item`
--
ALTER TABLE `booking_item`
MODIFY `booking_item_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hotel`
--
ALTER TABLE `hotel`
MODIFY `hotel_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
MODIFY `room_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
